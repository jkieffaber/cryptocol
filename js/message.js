    var p = ""; //prime number != q
    var q = ""; //prime number != p
    var n = ""; //p*q
    e = "10001";
    var d = "";
    var p = "";
    var dmp1 = "";
    var dmq1 = "";
    var coeff = "";
    
    //TODO delete temp variables for tested
    var ciphertext = "";

    function do_genrsa() {
	e = "10001";
	var before = new Date();
	var rsa = new RSAKey();
	rsa.generate(parseInt(2048),e);
	n = linebrk(rsa.n.toString(16),64);
	d = linebrk(rsa.d.toString(16),64);
	p = linebrk(rsa.p.toString(16),64);
	q = linebrk(rsa.q.toString(16),64);
	dmp1 = linebrk(rsa.dmp1.toString(16),64);
	dmq1 = linebrk(rsa.dmq1.toString(16),64);
	coeff = linebrk(rsa.coeff.toString(16),64);
	var after = new Date();
	
	$.ajax({
	    url: "publicKeyUpload.php",
	    type: "POST",
	    data: {
		publicKey:n,
		publicExp:e
	    },
	    dataType: "html",
	    success: function(data){
		//console.log(data);
    	  }
	});
    } //end do_genrsa
    
    function do_rsa_encrypt() {
	var current_target = 0;
        target_n = $.ajax({
	    url: "getKeys.php",
	    type: "POST",
	    global: false,
	    async: false,
	    data: {
		
	    },
	    dataType: "html",
	    success: function(data){
		return data;
    	  }
	}).responseText;
	
	//TODO make this able to handle multiple users in room
	//var targets = new Array();
	//var targetKeys = new Array();
	//var i = 0;
	////code to loop through json object and assign each target/publicKey to an array
	
	temp = JSON.parse(target_n);
	var targetId = temp.user_id;
	var targetKey = temp.publicKey;
	
	var before = new Date();
	var rsa = new RSAKey();
	rsa.setPublic(targetKey, e);
	var res = rsa.encrypt(document.getElementById('write').value);
	var after = new Date();
	if(res) {
	  ciphertext = linebrk(res, 64);
	}
	else{
	    console.log("***** Cipher Text not Created *****");
	}
	
	$.ajax({
	    url: "messages.php",
	    type: "POST",
	    data: {
		ciphertext: ciphertext,
		targetId: targetId
	    },
	    dataType: "html",
	    success: function(data){
		//console.log(data);
    	  }
	});
    }//end do_encrypt
    
function do_rsa_decrypt() {
	var ciphertext = $.ajax({
	    url: "checkMessages.php",
	    type: "POST",
	    global: false,
	    async: false,
	    data: {
		ciphertext: ciphertext
	    },
	    dataType: "html",
	    success: function(data){
		//console.log("console log in decrypt = " + data);
		return data;
    	  }
	}).responseText;
	//console.log("cipherText = " + ciphertext);
	//console.log("value of n in decrypt = " + n);
	
	var before = new Date();
	var rsa = new RSAKey();
	rsa.setPrivateEx(n, e, d, p, q, dmp1, dmq1, coeff);
	if(ciphertext == "") {
	    console.log("no ciphertext");
	    return;
	}
	var res = rsa.decrypt(ciphertext);
	var after = new Date();
	if(res == null) {
	    console.log("*** Decryption Unsuccessful ***");
	}
	else {
	    //var transcript = document.getElementById('transcript').value;
	    document.getElementById("transcript").value = res;
	    //console.log("decrypted data = " + res);
	    
	    //TODO add ajax call to delete.php to delete received messages
	}
    }//end do_decrypt