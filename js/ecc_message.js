// var rng;
 //var r = "";
// var n;
// var g;
// var curve;
// var h;
// var q;
// var a;
// var b;
// var gx;
// var gy;

//private key
var priv;

//public points
var pub_x;
var pub_y;

//private key
var key_x;
var key_y;

//aes key
var key;

//key generation global variables
var targetId;
var pointX;
var pointY;
var myUserId;

function do_genECC(){
	console.log("EC params set");
	set_secp256r1();
	
	rng = new SecureRandom();
	do_rand();
	do_pub();
	
	//you can't tell me what to do I'm a comment
	//send the public points (X,Y);
	$.ajax({
	    url: "eccKeyUpload.php",
	    type: "POST",
	    data: {
		pointX:pub_x,
		pointY:pub_y
	    },
	    dataType: "html",
	    success: function(data){
		console.log(data);
    	}
	});
}

function do_ecc_encrypt() {
    if (key != null) {
		var plaintext = document.getElementById('write').value;
		
		var encrypted_data = sjcl.encrypt(key, plaintext);
		
		console.log(encrypted_data);
	
		$.ajax({
			url: "ecc_messages.php",
			type: "POST",
			data: {
			    encrypted_data: encrypted_data,
			    targetId: targetId,
			    userID: myUserId
			},
			dataType: "html",
			success: function(data){
		    }
		});
	}
	else {
		alert("The room is currently empty");
	}
}//end do_encrypt


function do_key_poll() {
    target_n = $.ajax({
			url: "ecc_getKeys.php",
			type: "POST",
			global: false,
			async: false,
			data: {
			
			},
			dataType: "html",
			success: function(data){
				return data;
			}
		}).responseText;
		
		temp = JSON.parse(target_n);
		targetId = temp.user_id;
		pointX = temp.pointX;
		pointY = temp.pointY;
		myUserId = temp.myUserId;
		
		if (pointX != null && pointY != null) {
			//generate secret key based on other users public points (X,Y)
		    do_key(pointX, pointY);
		
			//set AES key
		    key = key_x + key_y;
		    console.log("pointX = " + pointX);
		    console.log("pointY = " + pointY);
		    console.log("key = " + key);
		}
		

}//end keyPoll


function do_ecc_decrypt() {
	var ciphertext = $.ajax({
	    url: "ecc_checkMessages.php",
	    type: "POST",
	    global: false,
	    async: false,
	    data: {
	    },
	    dataType: "html",
	    success: function(data){
		return data;
    	}
	}).responseText;
	
	var encryptedObject = ciphertext;
	encryptedObject = encryptedObject.replace(/\\/g, '');
	
	console.log(encryptedObject);
	//decrypt the message
	if (encryptedObject != "") {
		var msg = sjcl.decrypt(key, encryptedObject)
		document.getElementById("transcript").value = msg;
	}
		
}//end do_ecc_decrypt
	
function set_secp256r1() {
  set_ec_params("secp256r1");
}

function set_ec_params(name) {
  var c = getSECCurveByName(name);
  
  q = c.getCurve().getQ().toString();
  a = c.getCurve().getA().toBigInteger().toString();
  b = c.getCurve().getB().toBigInteger().toString();
  gx = c.getG().getX().toBigInteger().toString();
  gy = c.getG().getY().toBigInteger().toString();
  n = c.getN().toString();
}

function get_curve() {
  return new ECCurveFp(new BigInteger(q),
    new BigInteger(a),
    new BigInteger(b));
}

function get_G(curve) {
  return new ECPointFp(curve,
    curve.fromBigInteger(new BigInteger(gx)),
    curve.fromBigInteger(new BigInteger(gy)));
}

function pick_rand() {
  var nTemp = new BigInteger(n);
  var n1 = nTemp.subtract(BigInteger.ONE);
  var rTemp = new BigInteger(nTemp.bitLength(), rng);
  return rTemp.mod(n1).add(BigInteger.ONE);
}

function do_rand() {
  var r = pick_rand();
  priv = r.toString();
  pub_x = "";
  pub_y = "";
  key_x = "";
  key_y = "";
}

function do_pub() {
  if(priv.length == 0) {
    alert("Please generate a private value first");
    return;
  }
  var before = new Date();
  var curve = get_curve();
  var G = get_G(curve);
  var a = new BigInteger(priv);
  var P = G.multiply(a);
  var after = new Date();
  pub_x = P.getX().toBigInteger().toString();
  pub_y = P.getY().toBigInteger().toString();
}

function do_key(pointX, pointY) {
  if(priv.length == 0) {
    alert("Please generate a private value first");
    return;
  }
  if(pointX.length == 0) {
    alert("Please compute public value first");
    return;
  }
  var before = new Date();
  var curve = get_curve();
  var P = new ECPointFp(curve,
    curve.fromBigInteger(new BigInteger(pointX)),
    curve.fromBigInteger(new BigInteger(pointY)));
  var a = new BigInteger(priv);
  var S = P.multiply(a);
  var after = new Date();
  key_x = S.getX().toBigInteger().toString();
  key_y = S.getY().toBigInteger().toString();
}