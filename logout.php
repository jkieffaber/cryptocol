<?php
  require('db_connect.php');

  unset($_SESSION['logged_in']);  
  session_destroy();  

  $userId = $_COOKIE["userId"];
  $userId = $mysqli->real_escape_string($userId); 

  $mysqli->query("DELETE from Messages where user_id=".$userId.";");
  $mysqli->query("DELETE from Rooms where user_id=".$userId.";");
  $mysqli->query("DELETE from eccMessages where user_id=".$userId.";");
  $mysqli->query("DELETE from eccRooms where user_id=".$userId.";");

?>

<link rel='stylesheet' type='text/css' href='http://www.axiomcomputersecurity.com/wp-content/themes/responsive/style.css?ver=1.8.7' />

<h3 style="text-align: center; vertical-align: middle;">You are now logged out.</h3>

<h3 style="text-align: center; vertical-align: middle;">Thank you for using Cryptocol!</h3>

<?php

  $result->close();
  $mysqli->close();

?>
